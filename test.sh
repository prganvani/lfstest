
clear

echo 
[ -d ".git" ] && echo ".git is exists." || echo "Error: .git does not exists."

if [ -d ".git" ]; then

read -p "Enter Commit Message: " commitmsg
echo 

#To Commit Changes in Bitbucket Repo.
#Go into specific directory
#cd <path_to_local_repo>

#Add all modified files.
git add --all

#Commit all files which are added with specific message.
git commit -m $commitmsg

#Push all changes to repo.
git push

fi;